
package com.empresa.proyecto.ws;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.6-1b01 
 * Generated source version: 2.2
 * 
 */
@WebService(name = "PaisWebService", targetNamespace = "http://ws.proyecto.empresa.com/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface PaisWebService {


    /**
     * 
     * @param nomPais
     * @return
     *     returns java.lang.String
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "crearPaises", targetNamespace = "http://ws.proyecto.empresa.com/", className = "com.empresa.proyecto.ws.CrearPaises")
    @ResponseWrapper(localName = "crearPaisesResponse", targetNamespace = "http://ws.proyecto.empresa.com/", className = "com.empresa.proyecto.ws.CrearPaisesResponse")
    @Action(input = "http://ws.proyecto.empresa.com/PaisWebService/crearPaisesRequest", output = "http://ws.proyecto.empresa.com/PaisWebService/crearPaisesResponse")
    public String crearPaises(
        @WebParam(name = "nomPais", targetNamespace = "")
        String nomPais);

    /**
     * 
     * @return
     *     returns java.util.List<com.empresa.proyecto.ws.Pais>
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "listarPais", targetNamespace = "http://ws.proyecto.empresa.com/", className = "com.empresa.proyecto.ws.ListarPais")
    @ResponseWrapper(localName = "listarPaisResponse", targetNamespace = "http://ws.proyecto.empresa.com/", className = "com.empresa.proyecto.ws.ListarPaisResponse")
    @Action(input = "http://ws.proyecto.empresa.com/PaisWebService/listarPaisRequest", output = "http://ws.proyecto.empresa.com/PaisWebService/listarPaisResponse")
    public List<Pais> listarPais();

}
