
package com.empresa.proyecto.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.empresa.proyecto.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ListarPaisResponse_QNAME = new QName("http://ws.proyecto.empresa.com/", "listarPaisResponse");
    private final static QName _CrearPaises_QNAME = new QName("http://ws.proyecto.empresa.com/", "crearPaises");
    private final static QName _ListarPais_QNAME = new QName("http://ws.proyecto.empresa.com/", "listarPais");
    private final static QName _CrearPaisesResponse_QNAME = new QName("http://ws.proyecto.empresa.com/", "crearPaisesResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.empresa.proyecto.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CrearPaisesResponse }
     * 
     */
    public CrearPaisesResponse createCrearPaisesResponse() {
        return new CrearPaisesResponse();
    }

    /**
     * Create an instance of {@link ListarPais }
     * 
     */
    public ListarPais createListarPais() {
        return new ListarPais();
    }

    /**
     * Create an instance of {@link CrearPaises }
     * 
     */
    public CrearPaises createCrearPaises() {
        return new CrearPaises();
    }

    /**
     * Create an instance of {@link ListarPaisResponse }
     * 
     */
    public ListarPaisResponse createListarPaisResponse() {
        return new ListarPaisResponse();
    }

    /**
     * Create an instance of {@link Pais }
     * 
     */
    public Pais createPais() {
        return new Pais();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarPaisResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.proyecto.empresa.com/", name = "listarPaisResponse")
    public JAXBElement<ListarPaisResponse> createListarPaisResponse(ListarPaisResponse value) {
        return new JAXBElement<ListarPaisResponse>(_ListarPaisResponse_QNAME, ListarPaisResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CrearPaises }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.proyecto.empresa.com/", name = "crearPaises")
    public JAXBElement<CrearPaises> createCrearPaises(CrearPaises value) {
        return new JAXBElement<CrearPaises>(_CrearPaises_QNAME, CrearPaises.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarPais }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.proyecto.empresa.com/", name = "listarPais")
    public JAXBElement<ListarPais> createListarPais(ListarPais value) {
        return new JAXBElement<ListarPais>(_ListarPais_QNAME, ListarPais.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CrearPaisesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.proyecto.empresa.com/", name = "crearPaisesResponse")
    public JAXBElement<CrearPaisesResponse> createCrearPaisesResponse(CrearPaisesResponse value) {
        return new JAXBElement<CrearPaisesResponse>(_CrearPaisesResponse_QNAME, CrearPaisesResponse.class, null, value);
    }

}
